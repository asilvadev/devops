const request = require("supertest");

const baseURL = "https://api.github.com";
const basePath = "/users";

async function githubUser(user) {
  return request(baseURL).get(`${basePath}/${user}`);
}
module.exports = githubUser;
