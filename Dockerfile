FROM node:14.14.0

ENV NODE_ENV=testing

WORKDIR /src

COPY ["package.json", "./"]

