const { Given, When, Then } = require("@cucumber/cucumber");
const { expect } = require("chai");

const services = require("../../../services");

Given("that I want to check some valid user", async () => {
  this.user = "asilvadev";
});

Given("that I want to check some invalid user", async () => {
  this.user = "asilvadevINVALID";
});

When("I request the api", async () => {
  this.resp = await services.githubUser(this.user);
});

Then("I get the return infos about the user", async () => {
  expect(this.resp.status).to.equal(200);
  expect(this.resp.body.login).to.equal(this.user);
});

Then("I get error request", async () => {
  expect(this.resp.status).to.equal(404);
  expect(this.resp.body.message).to.equal("Not Found");
});
