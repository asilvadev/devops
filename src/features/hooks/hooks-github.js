const { AfterAll, After, Before } = require("@cucumber/cucumber");
require("dotenv").config({ path: `config/.env.${process.env}` });

Before(async (scenario) => {
  console.log(`\n=== Scenario: ${scenario.pickle.name} ===`);
});
