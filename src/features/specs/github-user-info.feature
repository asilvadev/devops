Feature:
  As an user
  I want to use Github's API
  To check some dev infos

  @devops
  Scenario: Check User
    Given that I want to check some valid user
    When I request the api
    Then I get the return infos about the user

  @devops
  Scenario: Check Invalid User
    Given that I want to check some invalid user
    When I request the api
    Then I get error request
